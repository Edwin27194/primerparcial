package com.mycompany.primeraserie;

import dto.Producto;
import rx.Observable;
import rx.observables.MathObservable;

import java.util.ArrayList;
import java.util.List;
public class PromedioProducto {
    public static void main(String[] args) {

        List<Producto> producto = new ArrayList<>();
        producto.add(new Producto("vehiculo_simple", 300));
        producto.add(new Producto("vehiculo_simple_aut", 300));
        producto.add(new Producto("vehiculo_doblet:traccion", 200));
        producto.add(new Producto("vehiculo_alta_gama", 800));
        producto.add(new Producto("motosicleta", 230));
        Observable<Producto> personObservable = Observable.from(producto);

        MathObservable
                .from(personObservable)
                //interceptor
                .averageInteger(Producto::getPrecio)
                .subscribe((promedio) -> {
                    System.out.println("PROMEDIO:" + promedio);
                });
    }
}
