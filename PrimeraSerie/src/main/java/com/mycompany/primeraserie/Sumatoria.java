package com.mycompany.primeraserie;
import dto.Producto;
import rx.Observable;
import rx.functions.Func2;

import java.util.ArrayList;
import java.util.List;
import rx.functions.Action1;
import rx.functions.Func1;
public class Sumatoria {
    public static void main(String[] args) {
        Integer[] precio = {300, 300, 200, 800, 230};


        List<Producto> producto = new ArrayList<>();
        producto.add(new Producto("vehiculo_simple", 300));
        producto.add(new Producto("vehiculo_simple_aut", 300));
        producto.add(new Producto("vehiculo_doblet:traccion", 200));
        producto.add(new Producto("vehiculo_alta_gama", 800));
        producto.add(new Producto("motosicleta", 230));


        Observable miobservable =
                Observable
                        
                        .from(producto.toArray())
                        
                        .map(new Func1<Object, Integer>() {
            @Override
            public Integer call(Object result) {
                Producto producto = (Producto) result;
                return producto.getPrecio();
            }
        })
                        
                        .reduce(
                                new Func2<Integer, Integer, Integer>() {
                                    @Override
                                    public Integer call(Integer acumulador, Integer actual) {
                                        
                                        return acumulador + actual;
                                    }
                                }
                        );

        miobservable.subscribe((sumatoria) -> {
            System.out.println("" +
                    "Sumatoria:" + sumatoria);
        });
    }
    
    
}
