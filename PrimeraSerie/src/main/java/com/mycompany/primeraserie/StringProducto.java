
package com.mycompany.primeraserie;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
public class StringProducto {
    public static void main(String[] args) {
        

        String[] productos = {"vehiculo_simple", "vehiculo_simple_aut", "vehiculo_doblet_traccion", "vehiculo_alta_gama","motocicleta"};
        Integer []precio={300,300,200,800,230};
       
        Observable.from(productos).filter(

                new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String t) {

                        return t.contains("a");
                    }

                }
        )
                .sorted()
                .subscribe(
                new Action1<String>() {

                    @Override
                    public void call(String s) {

                        System.out.println("producto:  " + s );                    

                    }
                    
                });
        
    }

}
