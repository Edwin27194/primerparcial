package com.mycompany.segundaserie;
import dto.Numeros;
import rx.Observable;
import rx.functions.Func2;

import java.util.ArrayList;
import java.util.List;
public class SumatoriaNumeros {
    public static void main(String[] args) {
        Integer[] numbers = {1, 2, 3, 4, 5,6,7,8};


        List<Numeros> numero = new ArrayList<>();
            numero.add(new Numeros(1, 2));
            numero.add(new Numeros(2, 5));
            numero.add(new Numeros(3, 6));
            numero.add(new Numeros(4, 8));
            numero.add(new Numeros(5, 10));
            numero.add(new Numeros(6, 35));
            numero.add(new Numeros(7, 2));
            numero.add(new Numeros(8, 10));


        Observable miobservable =
                Observable
                       
                        .from(numero.toArray())
                        
                        .map((result) -> {
                           
                            Numeros numeros = (Numeros) result;
                            return numeros.getNum();
                        })
                        
                        .reduce(
                                new Func2<Integer, Integer, Integer>() {
                                    @Override
                                    public Integer call(Integer acumulador, Integer actual) {
                                        
                                        return acumulador + actual;
                                    }
                                }
                        );

        miobservable.subscribe((sumatoria) -> {
            System.out.println("" +
                    "SUMATORIA:  " + sumatoria);
        });

     
    }
}
