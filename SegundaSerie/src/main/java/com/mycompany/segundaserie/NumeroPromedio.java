
package com.mycompany.segundaserie;
import dto.Numeros;
import rx.Observable;
import rx.observables.MathObservable;

import java.util.ArrayList;
import java.util.List;
public class NumeroPromedio {

    
    public static void main(String[] args) {
       List<Numeros> numero = new ArrayList<>();
        numero.add(new Numeros(1, 2));
        numero.add(new Numeros(2, 5));
        numero.add(new Numeros(3, 6));
        numero.add(new Numeros(4, 8));
        numero.add(new Numeros(5, 10));
        numero.add(new Numeros(6, 35));
        numero.add(new Numeros(7, 2));
        numero.add(new Numeros(8, 10));
        

        Observable<Numeros> personObservable = Observable.from(numero);

        MathObservable
                .from(personObservable)
                //interceptor
                .averageInteger(Numeros::getNum)
                .subscribe((promedio) -> {
                    System.out.println("PROMEDIO   :" + promedio);
                });
    }
    
    
}
